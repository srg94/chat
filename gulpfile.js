
/**
 * Created by vik_kod(vik_kod@mail.ru)
 */

'use strict';

const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const rename      = require('gulp-rename');
const sass        = require('gulp-sass');
const pug         = require('gulp-pug');
const del         = require('del');


/********************************************************************/
/*HTML***************************************************************/
/********************************************************************/

gulp.task('html', () => {
    return gulp.src('./src/pug/*.pug')
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
});


/********************************************************************/
/*IMG****************************************************************/
/********************************************************************/

gulp.task('img', () => {
    return gulp.src('./src/images/*')
        .pipe(gulp.dest('./dist/images/'))
        .pipe(browserSync.stream());
});


/********************************************************************/
/*FONTS**************************************************************/
/********************************************************************/

gulp.task('fonts', () => {
    return gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts/'))
        .pipe(browserSync.stream());
});


/********************************************************************/
/*CLEAN**************************************************************/
/********************************************************************/

gulp.task('clean', () => {
    return del(['./dist'], { read: false })
});

/********************************************************************/
/*STYLE:DEV**********************************************************/
/********************************************************************/

gulp.task('style', () => {
    return gulp.src('./src/style/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/style/'))
        .pipe(browserSync.stream());
});

/********************************************************************/
/*WATCH**************************************************************/
/********************************************************************/

gulp.task('watch', () => {
    gulp.watch('./src/images/*',                gulp.series('img'));
    gulp.watch('./src/**/*.pug',               gulp.series('html'));
    gulp.watch('./src/style/**/*.scss',       gulp.series('style'));
    gulp.watch('./src/style/**/*.*',          gulp.series('fonts'));
});

/********************************************************************/
/*SERVER*************************************************************/
/********************************************************************/

gulp.task('server', () => {
    return browserSync.init({
        server: {baseDir: './dist'},
        logPrefix: 'learn',
        logFileChanges: false,
        reloadDelay: 1000,
        ghostMode: false,
        online: true
    });
});

/********************************************************************/
/*DEV****************************************************************/
/********************************************************************/

gulp.task('default',
    gulp.series('clean', 'html','style', 'img', 'fonts',
        gulp.parallel('server', 'watch'))
);